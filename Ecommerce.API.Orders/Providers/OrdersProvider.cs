﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ecommerce.Api.Orders.Db;
using Ecommerce.Api.Orders.Db;
using Ecommerce.Api.Orders.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Order = Ecommerce.Api.Orders.Models.Order;

namespace Ecommerce.Api.Orders.Providers
{
    public class OrdersProvider : IOrdersProvider
    {
        private readonly OrdersDbContext dbContext;
        private readonly ILogger<OrdersProvider> logger;
        private readonly IMapper mapper;
        public OrdersProvider(OrdersDbContext dbContext, ILogger<OrdersProvider> logger, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.logger = logger;
            this.mapper = mapper;

            SeedData();
        }

        private void SeedData()
        {
            if (!dbContext.Orders.Any())
            {
                dbContext.OrderItems.Add(new OrderItem()
                    {Id = 1, OrderId = 1, ProductId = 1, Quantity = 1, UnitPrice = 20});
                dbContext.OrderItems.Add(new OrderItem()
                    { Id = 2, OrderId = 1, ProductId = 2, Quantity = 2, UnitPrice = 10 }); 
                dbContext.OrderItems.Add(new OrderItem()
                    { Id = 3, OrderId =4, ProductId = 3, Quantity = 3, UnitPrice = 300 });
                dbContext.Orders.Add(new Db.Order() {Id = 1, CustomerId = 1, OrderDate = DateTime.Now, Total = 2000});
                dbContext.Orders.Add(new Db.Order() { Id = 2, CustomerId = 2, OrderDate = DateTime.Now, Total = 3000 });

                dbContext.SaveChanges();
            }
        }

        public async Task<(bool IsSuccess, IEnumerable<Order> Order, string ErrorMessage)> GetOrdersAsync(int customerId)
        {
            try
            {
                var orders = await dbContext.Orders.Where(a=>a.CustomerId == customerId).ToListAsync();

                if (orders != null)
                {
                    orders.ForEach(order=> order.Items =  dbContext.OrderItems.Where(a => a.OrderId == order.Id).ToList());

                    var result = mapper.Map<IEnumerable<Db.Order>, IEnumerable<Models.Order>>(orders);
                    return (true, result, null);
                }

                return (false, null, "Not found");
            }
            catch (Exception ex)
            {
                logger?.LogError(ex.Message);
                return (false, null, ex.Message); ;
            }
        }
    }
}
