﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ecommerce.Api.Search.Interfaces;
using Ecommerce.Api.Search.Models;

namespace Ecommerce.Api.Search.Services
{
    public class SearchService : ISearchService
    {
        private readonly IOrdersService ordersService;
        private readonly IProductsService productService;
        private readonly ICustomersService customersService;

        public SearchService(IOrdersService ordersService, IProductsService productService, ICustomersService customersService)
        {
            this.ordersService = ordersService;
            this.productService = productService;
            this.customersService = customersService;
        }
        public async Task<(bool IsSuccess, dynamic SearchResults)> SearchAsync(int customerId)
        {
            var orderResult = await ordersService.GetOrdersAsync(customerId);
            var productsResult = await productService.GetProductAsync();

            
            if (orderResult.IsSuccess)
            {
                foreach (var order in orderResult.Orders)
                {
                    foreach (var item in order.Items)
                    {
                        item.ProductName = 
                            productsResult.IsSuccess ?
                            productsResult.Products.FirstOrDefault(p => p.Id == item.ProductId)?.Name : "Not Available";
                    }
                }

                var customerResult = await customersService.GetCustomersAsync(customerId);

                var result = new 
                {
                    Customer = customerResult.IsSuccess ? customerResult.Customer : new  { Name="Customer not available" },
                    Orders = orderResult.Orders
                };

                return (true, result);
            }

            return (false, null);
        }
    }
}
